#include <iostream>
#include <sstream>
#include <string>
#include "utilities.h"
#include "libpcap/pkt_sniffer.h"

bool controller_running;

int controller(const std::string host, int port){

    controller_running = true;

    while(controller_running){

        std::string cmd;
        std::cout << "Please enter a command to run at the destination: " << std::endl;
        std::getline (std::cin, cmd);
        std::cout << "Sent cmd: " << cmd  << std::endl;

        send(host, port, cmd);

        std::stringstream filter;
        filter << "udp and port " << port; 

        //receive results using amons libcap package
        std::string result;
        std::string host;
        start_sniffer(1, filter.str(), result, host);

        std::cout << "Results from: " << host << " : " << result << std::endl;
    }

    return 1;
}