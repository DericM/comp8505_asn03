CC          =g++
CLIB        =-std=c++11 -lpcap -lboost_system
CFLAG       =-Wall
PROG_NAME   =NotABackdoor

all: $(PROG_NAME)

$(PROG_NAME): main.o pkt_sniffer.o proc_ether.o proc_hdrs.o proc_payload.o
	$(CC) $(CFLAGS) -o bin/$(PROG_NAME) build/main.o build/pkt_sniffer.o build/proc_ether.o build/proc_hdrs.o build/proc_payload.o $(CLIB)

main.o: src/main.cpp
	$(CC) $(CFLAGS) -c src/main.cpp -o build/main.o $(CLIB)

#controler.o: src/controller.cpp pkt_sniffer.o 
#	$(CC) $(CFLAGS) -c src/controller.cpp -o build/controller.o build/pkt_sniffer.o $(CLIB)
#	
#backdoor.o: src/backdoor.cpp pkt_sniffer.o 
#	$(CC) $(CFLAGS) -c src/backdoor.cpp -o build/backdoor.o build/pkt_sniffer.o $(CLIB)
#pkt_sniffer: pkt_sniffer.o proc_ether.o proc_hdrs.o proc_payload.o
#	$(CC) -o pkt_sniffer build/pkt_sniffer.o build/proc_ether.o build/proc_hdrs.o build/proc_payload.o $(CLIB)

pkt_sniffer.o: proc_ether.o
	$(CC) -c src/libpcap/pkt_sniffer.cpp -o build/pkt_sniffer.o 

proc_ether.o: 
	$(CC) -c src/libpcap/proc_ether.cpp -o build/proc_ether.o
proc_hdrs.o: 
	$(CC) -c src/libpcap/proc_hdrs.cpp -o build/proc_hdrs.o
proc_payload.o: 
	$(CC) -c src/libpcap/proc_payload.cpp -o build/proc_payload.o



clean:
	rm -f bin/$(PROG_NAME) build/*.o